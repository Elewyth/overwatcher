using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OverwatcherUI {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        public List<string> Windows { get; } = new List<string>();

        public int OverwatchProcessId { get; private set; }

        private IntPtr owHandle;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);
        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        public MainWindow() {
            InitializeComponent();
            DataContext = this;

            Process[] processlist = Process.GetProcesses();
            foreach (var process in processlist) {
                if (!string.IsNullOrEmpty(process.MainWindowTitle)) {
                    if (process.MainWindowTitle == "Overwatch") {
                        Console.WriteLine("Overwatch found!");
                        OverwatchProcessId = process.Id;
                        owHandle = process.MainWindowHandle;
                        break;
                    }
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            if (owHandle == null || owHandle.Equals(IntPtr.Zero)) {
                Console.WriteLine("Overwatch not found!");
            } else {
                if (SetForegroundWindow(owHandle)) {
                    RECT srcRect;
                    if (!owHandle.Equals(IntPtr.Zero)) {
                        if (GetWindowRect(owHandle, out srcRect)) {
                            int width = srcRect.Right - srcRect.Left;
                            int height = srcRect.Bottom - srcRect.Top;

                            Bitmap bmp = new Bitmap(width, height);
                            Graphics screenG = Graphics.FromImage(bmp);

                            try {
                                screenG.CopyFromScreen(srcRect.Left, srcRect.Top,
                                        0, 0, new System.Drawing.Size(width, height),
                                        CopyPixelOperation.SourceCopy);

                                bmp.Save("overwatch.jpg", ImageFormat.Jpeg);
                            } catch (Exception ex) {
                                MessageBox.Show(ex.Message);
                            } finally {
                                screenG.Dispose();
                                bmp.Dispose();
                            }
                        }
                    }
                }
            }
        }
    }
}
